package ru.softstone.cvapp.domain.home

import com.arkivanov.mvikotlin.extensions.coroutines.SuspendExecutor
import ru.softstone.cvapp.domain.home.HomeStore.Action
import ru.softstone.cvapp.domain.home.HomeStore.Intent
import ru.softstone.cvapp.domain.home.HomeStore.Label
import ru.softstone.cvapp.domain.home.HomeStore.Result
import ru.softstone.cvapp.domain.home.HomeStore.State

class HomeExecutor : SuspendExecutor<Intent, Action, State, Result, Label>() {
    override suspend fun executeIntent(intent: Intent, getState: () -> State) = when(intent) {
        Intent.Increment -> {
            dispatch(Result.SetCount(getState().counter + 1))
        }
        Intent.NavigateNext -> publish(Label.NavigateNext)
    }
}
