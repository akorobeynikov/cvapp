package ru.softstone.cvapp.domain.test

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object TestDiModule {
    @Provides
    @ViewModelScoped
    internal fun provideTestStore(factory: TestStoreFactory) = factory.create()
}
