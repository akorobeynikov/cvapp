package ru.softstone.cvapp.domain.test

import com.arkivanov.mvikotlin.core.store.Reducer
import ru.softstone.cvapp.domain.test.TestStore.Result
import ru.softstone.cvapp.domain.test.TestStore.State

object TestReducer : Reducer<State, Result> {
    override fun State.reduce(result: Result) = when (result) {
        is Result.SetCount -> copy(count = result.count)
    }
}
