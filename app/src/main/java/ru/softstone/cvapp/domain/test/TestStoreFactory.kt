package ru.softstone.cvapp.domain.test

import com.arkivanov.mvikotlin.core.store.*
import ru.softstone.cvapp.domain.test.TestStore.*
import javax.inject.Inject

class TestStoreFactory @Inject constructor(
    private val storeFactory: StoreFactory
) {
    fun create(): TestStore =
        object : TestStore, Store<Intent, State, Label> by storeFactory.create(
            name = "TestStore",
            initialState = State(),
            executorFactory = { TestExecutor() },
            reducer = TestReducer,
            bootstrapper = SimpleBootstrapper(Action.Start)
        ) {}
}
