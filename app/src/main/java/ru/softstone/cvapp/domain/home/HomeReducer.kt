package ru.softstone.cvapp.domain.home

import com.arkivanov.mvikotlin.core.store.Reducer
import ru.softstone.cvapp.domain.home.HomeStore.Result
import ru.softstone.cvapp.domain.home.HomeStore.State

object HomeReducer : Reducer<State, Result> {
    override fun State.reduce(result: Result) = when (result) {
        is Result.SetCount -> copy(counter = result.count)
    }
}
