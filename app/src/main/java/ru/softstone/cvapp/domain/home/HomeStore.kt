package ru.softstone.cvapp.domain.home

import com.arkivanov.mvikotlin.core.store.Store
import ru.softstone.cvapp.domain.home.HomeStore.Intent
import ru.softstone.cvapp.domain.home.HomeStore.Label
import ru.softstone.cvapp.domain.home.HomeStore.State

interface HomeStore : Store<Intent, State, Label> {
    sealed class Intent {
        object Increment : Intent()
        object NavigateNext : Intent()
    }

    sealed class Action {
        object Start : Action()
    }

    data class State(
        val counter: Int = 0
    )

    sealed class Label {
        object NavigateNext : Label()
    }

    sealed class Result {
        data class SetCount(val count: Int): Result()
    }
}
