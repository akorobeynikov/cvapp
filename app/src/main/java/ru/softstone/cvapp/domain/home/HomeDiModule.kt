package ru.softstone.cvapp.domain.home

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object HomeDiModule {
    @Provides
    @ViewModelScoped
    internal fun provideHomeStore(factory: HomeStoreFactory) = factory.create()
}
