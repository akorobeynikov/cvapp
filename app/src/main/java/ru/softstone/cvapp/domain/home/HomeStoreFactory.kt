package ru.softstone.cvapp.domain.home

import com.arkivanov.mvikotlin.core.store.*
import ru.softstone.cvapp.domain.home.HomeStore.*
import javax.inject.Inject

class HomeStoreFactory @Inject constructor(
    private val storeFactory: StoreFactory
) {
    fun create(): HomeStore =
        object : HomeStore, Store<Intent, State, Label> by storeFactory.create(
            name = "HomeStore",
            initialState = State(),
            executorFactory = { HomeExecutor() },
            reducer = HomeReducer,
            bootstrapper = SimpleBootstrapper(Action.Start)
        ) {}
}
