package ru.softstone.cvapp.domain.test

import com.arkivanov.mvikotlin.core.store.Store
import ru.softstone.cvapp.domain.test.TestStore.Intent
import ru.softstone.cvapp.domain.test.TestStore.Label
import ru.softstone.cvapp.domain.test.TestStore.State

interface TestStore : Store<Intent, State, Label> {
    sealed class Intent {
    }

    sealed class Action {
        object Start : Action()
    }

    data class State(
        val count: Int = 0
    )

    sealed class Label {
    }

    sealed class Result {
        data class SetCount(val count: Int) : Result()
    }
}
