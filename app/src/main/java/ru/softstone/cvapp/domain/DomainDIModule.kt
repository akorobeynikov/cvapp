package ru.softstone.cvapp.domain

import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.main.store.DefaultStoreFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

@Module
@InstallIn(ActivityRetainedComponent::class)
object DomainDIModule {

    @Provides
    @ActivityRetainedScoped
    internal fun provideStoreFactory(): StoreFactory = DefaultStoreFactory
}
