package ru.softstone.cvapp.domain.test

import com.arkivanov.mvikotlin.extensions.coroutines.SuspendExecutor
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.softstone.cvapp.domain.test.TestStore.Action
import ru.softstone.cvapp.domain.test.TestStore.Intent
import ru.softstone.cvapp.domain.test.TestStore.Label
import ru.softstone.cvapp.domain.test.TestStore.Result
import ru.softstone.cvapp.domain.test.TestStore.State

class TestExecutor : SuspendExecutor<Intent, Action, State, Result, Label>() {
    override suspend fun executeAction(action: Action, getState: () -> State) = when(action) {
        Action.Start -> {
            var count = 0
            while (true) {
                dispatch(Result.SetCount(count++))
                delay(300)
            }
        }
    }
}
