package ru.softstone.cvapp.data

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.softstone.cvapp.data.core.DispatchersProvider
import ru.softstone.cvapp.data.core.DispatchersProviderImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataDiModule {
    @Provides
    @Singleton
    internal fun provideDispatchers(): DispatchersProvider = DispatchersProviderImpl
}
