package ru.softstone.cvapp.data.core

import kotlinx.coroutines.CoroutineDispatcher

interface DispatchersProvider {
    val unconfined: CoroutineDispatcher
    val default: CoroutineDispatcher
    val main: CoroutineDispatcher
    val io: CoroutineDispatcher
}
