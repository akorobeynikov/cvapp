package ru.softstone.cvapp.data.core

import kotlinx.coroutines.Dispatchers

object DispatchersProviderImpl : DispatchersProvider {
    override val unconfined = Dispatchers.Unconfined
    override val default = Dispatchers.Default
    override val main = Dispatchers.Main.immediate
    override val io = Dispatchers.IO
}
