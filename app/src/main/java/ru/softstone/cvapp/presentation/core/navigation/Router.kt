package ru.softstone.cvapp.presentation.core.navigation

import ru.softstone.cvapp.presentation.core.AppScreen

interface Router {
    suspend fun navigateTo(screen: AppScreen)
    suspend fun navigateBack()
}
