package ru.softstone.cvapp.presentation.home

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import ru.softstone.cvapp.data.core.DispatchersProvider
import ru.softstone.cvapp.domain.home.HomeStore
import ru.softstone.cvapp.domain.home.HomeStore.*
import ru.softstone.cvapp.presentation.core.navigation.Router
import ru.softstone.cvapp.presentation.core.viewmodel.BaseViewModel
import ru.softstone.cvapp.presentation.home.HomeView.Event
import ru.softstone.cvapp.presentation.home.HomeView.ViewState
import ru.softstone.cvapp.presentation.test.TestScreen
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class HomeViewModel @Inject constructor(
    store: HomeStore,
    dispatchers: DispatchersProvider,
    val view: HomeView,
    private val router: Router
) :
    BaseViewModel<Intent, State, Label, Event, ViewState, HomeView>(store, view, dispatchers) {
    override val stateToViewState: suspend State.() -> ViewState = {
        ViewState(counter.toString())
    }

    override val eventToIntent: suspend Event.() -> Intent = {
        when (this) {
            Event.Increment -> Intent.Increment
            Event.NavigateNext -> Intent.NavigateNext
        }
    }

    override fun handleLabel(label: Label) {
        when (label) {
            Label.NavigateNext -> viewModelScope.launch { router.navigateTo(TestScreen) }
        }
    }
}
