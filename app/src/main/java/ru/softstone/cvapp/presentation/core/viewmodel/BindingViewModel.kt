package ru.softstone.cvapp.presentation.core.viewmodel

import com.arkivanov.mvikotlin.core.binder.BinderLifecycleMode
import com.arkivanov.mvikotlin.core.lifecycle.doOnDestroy
import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.extensions.coroutines.bind
import kotlinx.coroutines.flow.Flow
import ru.softstone.cvapp.data.core.DispatchersProvider
import ru.softstone.cvapp.presentation.core.BaseView
import kotlin.coroutines.CoroutineContext

abstract class BindingViewModel<
        Intent : Any,
        State : Any,
        Label : Any,
        Event : Any,
        ViewState : Any,
        View : BaseView<ViewState, Event>>(
    protected val store: Store<Intent, State, Label>,
    private val view: View,
    private val dispatchersProvider: DispatchersProvider
) : LifecycleHolderViewModel() {

    init {
        check(!store.isDisposed) {
            "Store ${store::class.java.name} is disposed! Check DI scope"
        }
        binderLifecycle.doOnDestroy { store.dispose() }
    }

    fun bind() {
        bindFlows(view)
    }

    /**
     * Handle single action events
     */
    protected open fun handleLabel(label: Label) {
        // to override
    }

    private fun bindFlows(
        view: View,
        context: CoroutineContext = dispatchersProvider.main
    ) {
        bind(viewLifecycle, BinderLifecycleMode.START_STOP, context) {
            modelFlow() bindTo view
            labelFlow() bindTo { handleLabel(it) }
        }
        bind(viewLifecycle, BinderLifecycleMode.CREATE_DESTROY, context) {
            intentFlow(view) bindTo store
        }
    }

    abstract fun modelFlow(): Flow<ViewState>
    abstract fun labelFlow(): Flow<Label>
    abstract fun intentFlow(view: View): Flow<Intent>
}
