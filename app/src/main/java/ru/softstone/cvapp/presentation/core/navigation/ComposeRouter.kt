package ru.softstone.cvapp.presentation.core.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.NavController
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import ru.softstone.cvapp.presentation.core.AppScreen

class ComposeRouter : Router {
    private val commandsFlow = MutableSharedFlow<Command>(
        extraBufferCapacity = 10,
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )

    override suspend fun navigateTo(screen: AppScreen) {
        commandsFlow.emit(Command.NavigateTo(screen.label))
    }

    override suspend fun navigateBack() {
        commandsFlow.emit(Command.NavigateBack)
    }

    @Composable
    fun InitRouter(navController: NavController) {
        LaunchedEffect("navigation") {
            commandsFlow.onEach {
                when (it) {
                    is Command.NavigateTo -> navController.navigate(it.label)
                    Command.NavigateBack -> navController.popBackStack()
                }
            }.launchIn(this)
        }
    }

    private sealed class Command {
        data class NavigateTo(val label: String) : Command()
        object NavigateBack : Command()
    }
}
