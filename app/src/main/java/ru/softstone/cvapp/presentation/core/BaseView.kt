package ru.softstone.cvapp.presentation.core

import com.arkivanov.mvikotlin.core.view.BaseMviView
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

abstract class BaseView<ViewState : Any, Event : Any> : BaseMviView<ViewState, Event>() {

    private val mutableStateFlow by lazy { MutableStateFlow(getInitialState()) }
    val stateFlow: StateFlow<ViewState> get() = mutableStateFlow

    protected abstract fun getInitialState(): ViewState

    override fun render(model: ViewState) {
        MainScope().launch {
            mutableStateFlow.emit(model)
        }
    }
}
