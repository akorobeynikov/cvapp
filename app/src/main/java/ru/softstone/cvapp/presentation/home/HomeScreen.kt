package ru.softstone.cvapp.presentation.home

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.Lifecycle
import kotlinx.coroutines.ExperimentalCoroutinesApi
import ru.softstone.cvapp.presentation.core.AppScreen
import ru.softstone.cvapp.presentation.core.viewmodel.provideViewModel
import ru.softstone.cvapp.presentation.home.HomeView.Event.*

@ExperimentalCoroutinesApi
object HomeScreen : AppScreen("home") {
    @Composable
    override fun Content(lifecycle: Lifecycle) {
        with(provideViewModel<HomeViewModel>(lifecycle)) {
            HomeContent(
                view.stateFlow.collectAsState(),
                onIncrementClick = { view.dispatch(Increment) },
                onNextClick = { view.dispatch(NavigateNext) },
            )
        }
    }
}

@Composable
private fun HomeContent(
    state: State<HomeView.ViewState>,
    onIncrementClick: () -> Unit,
    onNextClick: () -> Unit,
) {
    Column() {
        Text(text = state.value.message)
        Button(onClick = onIncrementClick) {
            Text("+1")
        }
        Button(onClick = onNextClick) {
            Text("Navigate next")
        }
    }
}

