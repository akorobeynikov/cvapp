package ru.softstone.cvapp.presentation

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.components.SingletonComponent
import ru.softstone.cvapp.data.core.DispatchersProvider
import ru.softstone.cvapp.data.core.DispatchersProviderImpl
import ru.softstone.cvapp.presentation.core.navigation.ComposeRouter
import ru.softstone.cvapp.presentation.core.navigation.Router
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object PresentationsDiModule {

    @Provides
    @Singleton
    internal fun provideComposeRouter(): ComposeRouter = ComposeRouter()

    @Module
    @InstallIn(SingletonComponent::class)
    internal interface Bindings {
        @Binds
        @Singleton
        fun bindRouter(source: ComposeRouter): Router
    }
}
