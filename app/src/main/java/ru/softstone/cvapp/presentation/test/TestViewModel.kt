package ru.softstone.cvapp.presentation.test

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import ru.softstone.cvapp.data.core.DispatchersProvider
import ru.softstone.cvapp.domain.test.TestStore
import ru.softstone.cvapp.domain.test.TestStore.*
import ru.softstone.cvapp.presentation.core.navigation.Router
import ru.softstone.cvapp.presentation.core.viewmodel.SimpleViewModel
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class TestViewModel @Inject constructor(
    val view: TestView,
    val router: Router,
    store: TestStore,
    dispatchers: DispatchersProvider
) : SimpleViewModel<Intent, State, Label, TestView>(store, view, dispatchers)
