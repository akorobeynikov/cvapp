package ru.softstone.cvapp.presentation.core

import androidx.compose.runtime.Composable
import androidx.lifecycle.Lifecycle

abstract class AppScreen(val label: String) {
    @Composable
    abstract fun Content(lifecycle: Lifecycle)
}
