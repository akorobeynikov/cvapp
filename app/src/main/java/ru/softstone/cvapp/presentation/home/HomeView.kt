package ru.softstone.cvapp.presentation.home

import ru.softstone.cvapp.presentation.core.BaseView
import javax.inject.Inject

class HomeView @Inject constructor(): BaseView<HomeView.ViewState, HomeView.Event>() {
    override fun getInitialState() = ViewState()

    data class ViewState(val message: String = "")

    sealed class Event {
        object Increment : Event()
        object NavigateNext : Event()
    }
}
