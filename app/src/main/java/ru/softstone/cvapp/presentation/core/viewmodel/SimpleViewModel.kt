package ru.softstone.cvapp.presentation.core.viewmodel

import com.arkivanov.mvikotlin.core.store.Store
import kotlinx.coroutines.ExperimentalCoroutinesApi
import ru.softstone.cvapp.data.core.DispatchersProvider
import ru.softstone.cvapp.presentation.core.BaseView

@ExperimentalCoroutinesApi
abstract class SimpleViewModel<
        Intent : Any,
        State : Any,
        Label : Any,
        View : BaseView<State, Intent>
        >(
    store: Store<Intent, State, Label>,
    view: View,
    dispatchers: DispatchersProvider
) :
    BaseViewModel<Intent, State, Label, Intent, State, View>(
        store,
        view,
        dispatchers
    ) {
    override val stateToViewState: suspend State.() -> State = { this }
    override val eventToIntent: suspend Intent.() -> Intent = { this }
}
