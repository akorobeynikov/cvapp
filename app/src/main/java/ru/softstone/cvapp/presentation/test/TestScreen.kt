package ru.softstone.cvapp.presentation.test

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.Lifecycle
import kotlinx.coroutines.ExperimentalCoroutinesApi
import ru.softstone.cvapp.domain.test.TestStore
import ru.softstone.cvapp.presentation.core.AppScreen
import ru.softstone.cvapp.presentation.core.viewmodel.provideViewModel

@ExperimentalCoroutinesApi
object TestScreen : AppScreen("test") {
    @Composable
    override fun Content(lifecycle: Lifecycle) {
        with(provideViewModel<TestViewModel>(lifecycle)) {
            TestContent(
                state = view.stateFlow.collectAsState()
            )
        }
    }
}

@Composable
private fun TestContent(state: State<TestStore.State>) {
    Column() {
        Text(text = state.value.count.toString())
    }
}

