package ru.softstone.cvapp.presentation.core.viewmodel

import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.extensions.coroutines.events
import com.arkivanov.mvikotlin.extensions.coroutines.labels
import com.arkivanov.mvikotlin.extensions.coroutines.states
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.ExperimentalCoroutinesApi
import ru.softstone.cvapp.data.core.DispatchersProvider
import ru.softstone.cvapp.presentation.core.BaseView

@ExperimentalCoroutinesApi
abstract class BaseViewModel<
        Intent : Any,
        State : Any,
        Label : Any,
        Event : Any,
        ViewState : Any,
        View : BaseView<ViewState, Event>
        >(
    store: Store<Intent, State, Label>,
    view: View,
    dispatchers: DispatchersProvider
) :
    BindingViewModel<Intent, State, Label, Event, ViewState, View>(
        store,
        view,
        dispatchers
    ) {
    /**
     * Converts any feature state as State to View's Model
     */
    protected abstract val stateToViewState: suspend (State.() -> ViewState)

    /**
     * Converts any action as Event to Store's Intent
     */
    protected abstract val eventToIntent: suspend (Event.() -> Intent)

    override fun modelFlow() = store.states.map(stateToViewState)
    override fun labelFlow() = store.labels
    override fun intentFlow(view: View) = view.events.map(eventToIntent)
}
