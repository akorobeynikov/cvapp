package ru.softstone.cvapp.presentation.core.navigation

import androidx.navigation.NamedNavArgument
import androidx.navigation.NavDeepLink
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import ru.softstone.cvapp.presentation.core.AppScreen

fun NavGraphBuilder.screen(
    screen: AppScreen,
    arguments: List<NamedNavArgument> = emptyList(),
    deepLinks: List<NavDeepLink> = emptyList(),
) {
    composable(screen.label, arguments, deepLinks) { screen.Content(it.lifecycle) }
}
