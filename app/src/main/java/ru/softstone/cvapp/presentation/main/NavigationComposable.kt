package ru.softstone.cvapp.presentation.main

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.ExperimentalCoroutinesApi
import ru.softstone.cvapp.presentation.core.navigation.ComposeRouter
import ru.softstone.cvapp.presentation.core.navigation.screen
import ru.softstone.cvapp.presentation.home.HomeScreen
import ru.softstone.cvapp.presentation.test.TestScreen

@ExperimentalCoroutinesApi
@Composable
fun NavigationComposable(router: ComposeRouter) {
    val navController = rememberNavController()
    router.InitRouter(navController)
    NavHost(navController, startDestination = HomeScreen.label) {
        screen(HomeScreen)
        screen(TestScreen)
    }
}
