package ru.softstone.cvapp.presentation.test

import ru.softstone.cvapp.domain.test.TestStore
import ru.softstone.cvapp.presentation.core.BaseView
import javax.inject.Inject

class TestView @Inject constructor(): BaseView<TestStore.State, TestStore.Intent>() {
    override fun getInitialState() = TestStore.State()
}
